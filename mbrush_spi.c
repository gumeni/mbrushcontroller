/*
 * spi.c
 *
 *  Created on: 3 lis 2021
 *      Author: dkopec
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "mbrush_spi.h"
#include "lcd44780.h"
#include "defines.h"
#include "uart.h"

extern volatile uint8_t move_x, unknown, reflection, spi_byte_idx;

void mbrush_spi_init(void){
	// enable, sck high idle, sample traling(last) edge
	SPCR |= (1 << SPE) | (1 << CPOL) | (1 << CPHA) | (1<<SPR0); // sprobowac usunac spr0
//	SPCR |= (1 << SPIE) | (1 << SPE) | (1 << CPHA) | (1<<SPR0); // sprobowac usunac spr0
//	SPCR |= (1 << SPIE) | (1 << SPE) | (1 << CPOL) | (1<<SPR0); // sprobowac usunac spr0
//	SPCR |= (1 << SPIE) | (1 << SPE) | (1<<SPR0); // sprobowac usunac spr0, w ogole nie dzia�a
	SPSR |= (1 << SPI2X); // spr�bowac wylaczyc

	MBRUSH_TRIGGER_DIR_OUT;
	MBRUSH_MOSI_EN_DIR_OUT;
	MBRUSH_SPI_MISO_DIR_OUT;
	MBRUSH_DATA_DIR_OUT;
	MBRUSH_TRIGGER_HIGH;
	MBRUSH_MOSI_EN_HIGH;

//	SPDR = 0;
}

//void mbrush_spi_send_byte(uint8_t byte){
//	MBRUSH_DATA_DIR_HIGH;
//	MBRUSH_MOSI_EN_LOW;
//	SPDR = byte;
//}
//
//uint8_t mbrush_spi_read_byte(void){
//	return SPDR;
//}

void mbrush_spi_enable(void){
	SPCR |= (1 << SPIE);
}

void mbrush_spi_disable(void){
	SPCR &= ~(1 << SPIE);
}

ISR(SPI_STC_vect){
	static uint8_t read_byte;

	MBRUSH_MOSI_EN_HIGH;
	MBRUSH_DATA_DIR_LOW;

//	read_byte = mbrush_spi_read_byte();
	read_byte = SPDR;

	if(!(spi_byte_idx % 2)){
		MBRUSH_DATA_DIR_HIGH;
		MBRUSH_MOSI_EN_LOW;
		if(read_byte == MBRUSH_CMD_UNKNOWN) SPDR = unknown;// mbrush_spi_send_byte(move_x_low);
		else if(read_byte == MBRUSH_CMD_MOVE_X) SPDR = move_x;// mbrush_spi_send_byte(move_x_high);
		else if(read_byte == MBRUSH_CMD_REFLECTION){
			SPDR = reflection;
			//mbrush_spi_send_byte(reflection);
//			spi_send = 1;
		}
	}
	spi_byte_idx++;
	if(spi_byte_idx > 5) spi_byte_idx = 0;
}
