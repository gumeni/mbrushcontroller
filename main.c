/*
 * main.c
 *
 *  Created on: 3 lis 2021
 *      Author: dkopec
 */

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "function.h"
#include "lcd44780.h"
#include "mbrush_spi.h"


volatile char chr;
volatile uint8_t hold, unknown, move_x, reflection, trigger_req, photo_detect, spi_byte_idx;
volatile uint8_t interval_int, interval_cnt_int, interval_send, printing_status, work_mode;
volatile uint16_t photo_delay_cnt;
uint8_t photo_delay_high, photo_delay_low;



int main(void){

	device_init();

	while(1){

		menu_fun();

	}
}
