/*
 * function.c
 *
 *  Created on: 22 lis 2021
 *      Author: dkopec
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>

#include "defines.h"
#include "lcd44780.h"
#include "mbrush_spi.h"
#include "uart.h"

uint8_t ee_unknown EEMEM = 0x81;
uint8_t ee_move_x EEMEM = 0x02;
uint8_t ee_reflection EEMEM = 0x80;
uint8_t ee_photo_delay_high EEMEM = 0x05;
uint8_t ee_photo_delay_low EEMEM = 0x00;
uint8_t ee_interval_int EEMEM = 0x01;
uint8_t ee_work_mode EEMEM = 0x00;

extern volatile uint8_t chr, hold, unknown, move_x, reflection, photo_detect, spi_byte_idx;
extern volatile uint8_t interval_int, interval_cnt_int, interval_send, printing_status, work_mode;
extern uint8_t photo_delay_high, photo_delay_low;
extern uint16_t photo_delay_cnt;

enum Mode {
	none, menu_0, print, printing, settings_parameters_0, settings_parameter_values_0, settings_parameter_values_1,
	settings_parameter_times_0, settings_parameter_times_1, settings_parameters_1, settings_parameters_1_work_mode
};


static void eeprom_variable_init(){
	unknown = eeprom_read_byte(&ee_unknown);
	move_x = eeprom_read_byte(&ee_move_x);
	reflection = eeprom_read_byte(&ee_reflection);
	photo_delay_high = eeprom_read_byte(&ee_photo_delay_high);
	photo_delay_low = eeprom_read_byte(&ee_photo_delay_low);
	interval_int = eeprom_read_byte(&ee_interval_int);
	work_mode = eeprom_read_byte(&ee_work_mode);
	if(work_mode > 1) work_mode = 0;
}


void device_init(void){

//	wdt_enable(WDTO_500MS);
//	WDTCSR &= ~(1<<WDE);
	wdt_disable();

	TIMSK0 |= (1 << TOIE0);
	TCCR0B |= (1 << CS02) | (1 << CS00);

	TCCR1B |= (1 << WGM12) | (1 << CS12) | (1 << CS10); // prescaler 1024

	TCCR2B |= (1 << CS22) | (1 << CS21) | (1 << CS20); // prescaler 1024
	TIMSK2 |= (1 << TOIE2);

	ROW_0_OUT;
	ROW_1_OUT;
	ROW_2_OUT;
	ROW_3_OUT;
	ROW_4_OUT;
	COL_0_PULLUP;
	COL_1_PULLUP;
	COL_2_PULLUP;
	COL_3_PULLUP;
	MBRUSH_BTN_OUT;
	LCD_BKG_OUT;
	LCD_BKG_ON;

	PHOTO_PULLUP;
	ENC_A_PULLUP;
	ENC_B_PULLUP;

	ST_LED_OUT;
	ST_LED_RED;
	lcd_init();
	mbrush_spi_init();
	eeprom_variable_init();
	uart0_init(UART_BAUD_SELECT(115200, F_CPU));

	sei();

	lcd_locate(0, 5);
	lcd_str("MBrush");
	lcd_locate(1, 3);
	lcd_str("Controller");
	_delay_ms(500);
	ST_LED_GREEN;
	_delay_ms(500);
	ST_LED_BLUE;
	_delay_ms(500);
	ST_LED_GREEN;
	_delay_ms(1500);
	lcd_locate(0,0);
	lcd_str("HW Version: ");
	lcd_int(VERSION_HW_H);
	lcd_char('.');
	lcd_int(VERSION_HW_L);

	lcd_locate(1, 0);
	lcd_str("FW Version: ");
	lcd_int(VERSION_FW_H);
	lcd_char('.');
	lcd_int(VERSION_FW_L);
	_delay_ms(3000);
}

static void saved(uint8_t *address, uint8_t value){
	cli();
	eeprom_update_byte(address, value);
	sei();
	lcd_cls();
	lcd_locate(0, 4);
	lcd_str_P(PSTR("Zapisano"));
	_delay_ms(1000);
}

static void choose(
		uint8_t *tick,
		enum Mode *mode,
		enum Mode mode_before,
		enum Mode mode_back,
		enum Mode mode_next,
		enum Mode mode_0,
		enum Mode mode_1){

	if(chr != 0){
		if(chr == '^'){
			if(*tick){
				lcd_locate(*tick, 0);
				lcd_char(' ');
				*tick = *tick - 1;
				lcd_locate(*tick, 0);
				lcd_char('>');
			}
			else if(mode_back != none){
				*tick = 1;
				*mode = mode_back;
			}
		}
		else if(chr == 'O'){
			if(*tick == 0) *mode = mode_0;
			else if(*tick == 1){
				*mode = mode_1;
				*tick = 0;
			}
		}
		else if(chr == 'V'){
			if(*tick < 1 && mode_1 != none){
				lcd_locate(*tick, 0);
				lcd_char(' ');
				*tick = *tick + 1;
				lcd_locate(*tick, 0);
				lcd_char('>');
			}
			else if(mode_next != none){
				*tick = 0;
				*mode = mode_next;
			}
		}
		else if(chr == 'N'){
			*tick = 0;
			*mode = mode_before;
		}
		chr=0;
	}
}

static void set_values(
		uint8_t *tick,
		enum Mode *mode,
		enum Mode *current_mode,
		enum Mode mode_before,
		enum Mode mode_back,
		enum Mode mode_next,
		uint8_t *address_0,
		uint8_t *address_1,
		volatile uint8_t *value_0,
		volatile uint8_t *value_1){


	uint8_t value = 0, position = 100;

	if(chr != 0){
		if(chr == '^'){
			if(*tick){
				lcd_locate(*tick, 0);
				lcd_char(' ');
				*tick = *tick - 1;
				lcd_locate(*tick, 0);
				lcd_char('>');
			}
			else if(mode_back != none){
				*tick = 1;
				*mode = mode_back;
			}
		}
		else if(chr == 'O'){
			chr=0;
			lcd_locate(*tick, 13);
			lcd_blink_on();
			while(1){
				if(chr !=0 ){
					if(chr >= '0' && chr <= '9' && position){
						if(!((position == 100 && chr > '2') ||
								(value == 200 && chr > '5') ||
								(value == 250 && chr > '5'))){

							lcd_char(chr);
							value = value + (chr-48) * position;
							position = position / 10;
						}
					}
					else if(chr == 'O'){
						lcd_blink_off();
						if(*tick){
							*value_1 = value;
							saved(address_1, *value_1);
						}
						else{
							*value_0 = value;
							saved(address_0, *value_0);
						}
						*current_mode = none;
						break;
					}
					else if(chr == 'N'){
						*current_mode = none;
						break;
					}
					chr=0;
				}
			}
			lcd_blink_off();
		}
		else if(chr == 'V'){
			if(*tick < 1 && address_1 != 0){
				lcd_locate(*tick, 0);
				lcd_char(' ');
				*tick = *tick + 1;
				lcd_locate(*tick, 0);
				lcd_char('>');
			}
			else if(mode_next != none){
				*tick = 0;
				*mode = mode_next;
			}
		}
		else if(chr == 'N'){
			*tick = 0;
			*mode = mode_before;
		}
		chr=0;
	}
}

static void display_options(uint8_t *tick, const char *option_0, const char *option_1){
		lcd_cls();
		lcd_locate(*tick, 0);
		lcd_char('>');
		lcd_locate(0,1);
		lcd_str_P(option_0);
		lcd_locate(1,1);
		lcd_str_P(option_1);
}

static void display_value(uint8_t position, uint8_t value){
		lcd_locate(position, 13);
		if(value < 100) lcd_char('0');
		if(value < 10) lcd_char('0');
		lcd_int(value);
}


void menu_fun(void){
	static enum Mode mode = menu_0;
//	static enum Mode mode = print;
	static enum Mode current_mode = none;
	static uint8_t tick, blink_timer;

	while(mode == menu_0){
		if(current_mode != mode){
			display_options(&tick, PSTR("Drukowanie"), PSTR("Ustaw. param."));
			current_mode = mode;
		}
		choose(
				&tick,
				&mode,
				menu_0, none, none,
				print, settings_parameters_0
		);
	}
	while(mode == print){
		if(current_mode != mode){
			lcd_cls();
			lcd_locate(0,0);

			lcd_str("Int ");
			if(interval_int < 100) lcd_char('0');
			if(interval_int < 10) lcd_char('0');
			lcd_int(interval_int);

			lcd_str(" X ");
			if(move_x < 100) lcd_char('0');
			if(move_x < 10) lcd_char('0');
			lcd_int(move_x);

			lcd_locate(1,0);
			lcd_str("OpH ");
			if(photo_delay_high < 100) lcd_char('0');
			if(photo_delay_high < 10) lcd_char('0');
			lcd_int(photo_delay_high);

			lcd_str(" OpL ");
			if(photo_delay_low < 100) lcd_char('0');
			if(photo_delay_low < 10) lcd_char('0');
			lcd_int(photo_delay_low);

			current_mode = mode;
		}
		if(chr != 0){
			if(chr == 'S'){
				mode = printing;
			}
			else if(chr == 'N'){
				mode = menu_0;
			}
			chr=0;
		}
	}
	while(mode == printing){
		if(current_mode != mode){

			PORT(ROW_0_PORT) |= (1<<ROW_0_PIN);
			PORT(ROW_1_PORT) &= ~(1<<ROW_1_PIN);
			PORT(ROW_2_PORT) |= (1<<ROW_2_PIN);
			PORT(ROW_3_PORT) |= (1<<ROW_3_PIN);
			PORT(ROW_4_PORT) |= (1<<ROW_4_PIN);
			_delay_us(100);

			TIMSK0 &= ~(1 << TOIE0);
			TIMSK2 &= ~(1 << TOIE2);
			EICRA |= (1 << ISC21);
			EIMSK |= (1 << INT2);

			if(work_mode){ // enc
				EICRA |= /*(1 << ISC11) |*/ (1 << ISC01);
				EIMSK |= /*(1 << INT1) |*/ (1 << INT0);
				TIMSK1 &= ~(1 << OCIE1B);
				OCR1A = 44; //interwa� wysy�ania, prescaler 8, timer 16bit 20us
				TCCR1B |= (1 << CS11); // prescaler 8
				TCCR1B &= ~(1 << CS12) & ~(1 << CS10);
			}

			printing_status = 0;
			interval_send = 0;
			photo_delay_cnt = 0;
			photo_detect = 0;
			current_mode = mode;
			mbrush_spi_enable();
			lcd_locate(0, 15);
			lcd_char('P');
		}

		if(photo_detect == 1){
			ST_LED_BLUE;
//			TIMSK1 &= ~(1 << OCIE1A); // off trigger time count
//			OCR1A = 17; // 1ms interrupt for photo detect delay

			uint16_t photo_delay = ((uint16_t)photo_delay_high << 8) + photo_delay_low;
			if(!work_mode){ // time
				photo_delay_cnt = (photo_delay > (MBRUSH_BTN_TIME_MS / 10)) ? photo_delay - (MBRUSH_BTN_TIME_MS / 10) : 0;
				TIMSK1 &= ~(1 << OCIE1A); // off trigger time count
				TCCR1B |= (1 << CS12) | (1 << CS10); // prescaler 1024
				TCCR1B &= ~(1 << CS11);
				OCR1A = 179; // 10ms interrupt for photo detect delay
				TCNT1 = 0;
				TIMSK1 |= (1 << OCIE1B); // on photo_delay_cnt time
			}
			else{
				photo_delay_cnt = photo_delay;
				interval_send = 0;
			}

			MBRUSH_BTN_HIGH;
			_delay_ms(MBRUSH_BTN_TIME_MS);
			MBRUSH_BTN_LOW;
			photo_detect = 2;
		}

		if(photo_detect == 2){
			if(!photo_delay_cnt){
	//			TIMSK1 &= ~(1 << OCIE1B); // off photo_delay_cnt time

	//			OCR1A = (interval_int * 18) - 1; //interwa� wysy�ania, prescaler 1024, timer 16bit

				if(!work_mode){ // time
					TCCR1B |= (1 << CS11); // prescaler 8
					TCCR1B &= ~(1 << CS12) & ~(1 << CS10);
//					OCR1A = (interval_int * 22) - 1; //interwa� wysy�ania, prescaler 8, timer 16bit 10us * x
					OCR1A = 44; //interwa� wysy�ania, prescaler 8, timer 16bit 20us
					TIMSK1 &= ~(1 << OCIE1B); // off photo_delay_cnt time
					TCNT1 = 0;
					TIMSK1 |= (1 << OCIE1A); // on trigger time count, interwa� wysy�ania
				}
				else{
					interval_cnt_int = interval_int;
					interval_send = 1;
					TCNT1 = 0;
				}

	//			EICRA &= ~(1 << ISC21);
	//			EIMSK &= ~(1 << INT2);
				photo_detect = 3;
				ST_LED_GREEN;
			}
		}

		if((PIN(PHOTO_PORT) & (1 << PHOTO_PIN)) && photo_detect == 3) photo_detect = 0;

//		ST_LED_TOG;
//		if(spi_send == 1){
//			lcd_locate(1, 15);
//			lcd_char('0');
//			spi_send = 2;
//		}

//		if(!printing_status){
//			lcd_locate(0, 15);
//			lcd_char('P');
//		}
//		else{
//			lcd_locate(0, 15);
//			lcd_char(' ');
//		}

		if(!(PIN(COL_3_PORT) & (1<<COL_3_PIN))){
			mode = print;
			mbrush_spi_disable();
			TIMSK1 &= ~(1 << OCIE1A) & ~(1 << OCIE1B);
			EICRA &= ~(1 << ISC21) & ~(1 << ISC11) & ~(1 << ISC01);
			EIMSK &= ~(1 << INT2) & ~(1 << INT1) & ~(1 << INT0);
			uart0_flush();
			TIMSK0 |= (1 << TOIE0);
			TIMSK2 |= (1 << TOIE2);
			lcd_locate(0, 15);
			lcd_char(' ');
		}
	}
	while(mode == settings_parameters_0){
		if(current_mode != mode){
			display_options(&tick, PSTR("Param. druku"), PSTR("Param. ster."));
			current_mode = mode;
		}
		choose(
				&tick,
				&mode,
				menu_0, none, settings_parameters_1,
				settings_parameter_values_0, settings_parameter_times_0
		);
	}
	while(mode == settings_parameter_values_0){
		if(current_mode != mode){
			display_options(&tick, PSTR("Przes. X"), PSTR("Nieznany"));
			display_value(0, move_x);
			display_value(1, unknown);
			current_mode = mode;
		}
		set_values(
				&tick,
				&mode, &current_mode,
				settings_parameters_0, none, settings_parameter_values_1,
				&ee_move_x, &ee_unknown,
				&move_x, &unknown
		);
	}
	while(mode == settings_parameter_values_1){
		if(current_mode != mode){
			display_options(&tick, PSTR("Odbijalnosc"), PSTR(""));
			display_value(0, reflection);
			current_mode = mode;
		}
		set_values(
				&tick,
				&mode, &current_mode,
				settings_parameters_0, settings_parameter_values_0, none,
				&ee_reflection, 0,
				&reflection, 0
		);
	}
	while(mode == settings_parameter_times_0){
		if(current_mode != mode){
			display_options(&tick, PSTR("OpH. foto."), PSTR("OpL. foto."));
			display_value(0, photo_delay_high);
			display_value(1, photo_delay_low);
			current_mode = mode;
		}
		set_values(
				&tick,
				&mode, &current_mode,
				settings_parameters_0, none, settings_parameter_times_1,
				&ee_photo_delay_high, &ee_photo_delay_low,
				&photo_delay_high, &photo_delay_low
		);
	}
	while(mode == settings_parameter_times_1){
		if(current_mode != mode){
			display_options(&tick, PSTR("Inter. wys."), PSTR(""));
			display_value(0, interval_int);
			current_mode = mode;
		}
		set_values(
				&tick,
				&mode, &current_mode,
				settings_parameters_0, settings_parameter_times_0, none,
				&ee_interval_int, 0,
				&interval_int, 0
		);
	}
	while(mode == settings_parameters_1){
		if(current_mode != mode){
			display_options(&tick, PSTR("Tryb pracy"), PSTR(""));
			lcd_locate(0, 13);
			if(work_mode){
				lcd_str_P(PSTR("ENC"));
			}
			else{
				lcd_str_P(PSTR("TIM"));
			}
			current_mode = mode;
		}
		choose(
				&tick,
				&mode,
				menu_0, settings_parameters_0, none,
				settings_parameters_1_work_mode, none
		);
	}
	while(mode == settings_parameters_1_work_mode){
		if(current_mode != mode){
			current_mode = mode;
		}

		lcd_locate(0, 13);
		if(!blink_timer){
			blink_timer = 200;
			if(work_mode){
				lcd_str_P(PSTR("ENC"));
			}
			else{
				lcd_str_P(PSTR("TIM"));
			}
		}
		else if(blink_timer == 100){
			lcd_str_P(PSTR("   "));
		}
		blink_timer--;
		_delay_ms(2);


		if(chr != 0){
			lcd_locate(0, 13);
			if(chr == '^'){
				if(!work_mode) work_mode++;
			}
			else if(chr == 'O'){
				saved(&ee_work_mode, work_mode);
				mode = settings_parameters_1;
			}
			else if(chr == 'V'){
				if(work_mode) work_mode--;
			}
			else if(chr == 'N'){
				tick = 0;
				mode = settings_parameters_1;
			}

			if(work_mode){
				lcd_str_P(PSTR("ENC"));
			}
			else{
				lcd_str_P(PSTR("TIM"));
			}
			blink_timer = 200;
			chr=0;
		}
	}
}


ISR(TIMER0_OVF_vect){
	static uint8_t hold_time = 0;

	if(!hold || hold_time == 100){
		PORT(ROW_0_PORT) &= ~(1<<ROW_0_PIN);
		PORT(ROW_1_PORT) |= (1<<ROW_1_PIN);
		PORT(ROW_2_PORT) |= (1<<ROW_2_PIN);
		PORT(ROW_3_PORT) |= (1<<ROW_3_PIN);
		PORT(ROW_4_PORT) |= (1<<ROW_4_PIN);
		_delay_us(100);
		if(!(PIN(COL_0_PORT) & (1<<COL_0_PIN))) chr = '1';
		else if(!(PIN(COL_1_PORT) & (1<<COL_1_PIN))) chr = '2';
		else if(!(PIN(COL_2_PORT) & (1<<COL_2_PIN))) chr = '3';
		else if(!(PIN(COL_3_PORT) & (1<<COL_3_PIN))) chr = 'S';

		PORT(ROW_0_PORT) |= (1<<ROW_0_PIN);
		PORT(ROW_1_PORT) &= ~(1<<ROW_1_PIN);
		PORT(ROW_2_PORT) |= (1<<ROW_2_PIN);
		PORT(ROW_3_PORT) |= (1<<ROW_3_PIN);
		PORT(ROW_4_PORT) |= (1<<ROW_4_PIN);
		_delay_us(100);
		if(!(PIN(COL_0_PORT) & (1<<COL_0_PIN))) chr = '4';
		else if(!(PIN(COL_1_PORT) & (1<<COL_1_PIN))) chr = '5';
		else if(!(PIN(COL_2_PORT) & (1<<COL_2_PIN))) chr = '6';
		else if(!(PIN(COL_3_PORT) & (1<<COL_3_PIN))) chr = 'P';

		PORT(ROW_0_PORT) |= (1<<ROW_0_PIN);
		PORT(ROW_1_PORT) |= (1<<ROW_1_PIN);
		PORT(ROW_2_PORT) &= ~(1<<ROW_2_PIN);
		PORT(ROW_3_PORT) |= (1<<ROW_3_PIN);
		PORT(ROW_4_PORT) |= (1<<ROW_4_PIN);
		_delay_us(100);
		if(!(PIN(COL_0_PORT) & (1<<COL_0_PIN))) chr = '7';
		else if(!(PIN(COL_1_PORT) & (1<<COL_1_PIN))) chr = '8';
		else if(!(PIN(COL_2_PORT) & (1<<COL_2_PIN))) chr = '9';
		else if(!(PIN(COL_3_PORT) & (1<<COL_3_PIN))) chr = 'O';

		PORT(ROW_0_PORT) |= (1<<ROW_0_PIN);
		PORT(ROW_1_PORT) |= (1<<ROW_1_PIN);
		PORT(ROW_2_PORT) |= (1<<ROW_2_PIN);
		PORT(ROW_3_PORT) &= ~(1<<ROW_3_PIN);
		PORT(ROW_4_PORT) |= (1<<ROW_4_PIN);
		_delay_us(100);
		if(!(PIN(COL_0_PORT) & (1<<COL_0_PIN))) chr = '*';
		else if(!(PIN(COL_1_PORT) & (1<<COL_1_PIN))) chr = '0';
		else if(!(PIN(COL_2_PORT) & (1<<COL_2_PIN))) chr = '#';
		else if(!(PIN(COL_3_PORT) & (1<<COL_3_PIN))) chr = 'N';

		PORT(ROW_0_PORT) |= (1<<ROW_0_PIN);
		PORT(ROW_1_PORT) |= (1<<ROW_1_PIN);
		PORT(ROW_2_PORT) |= (1<<ROW_2_PIN);
		PORT(ROW_3_PORT) |= (1<<ROW_3_PIN);
		PORT(ROW_4_PORT) &= ~(1<<ROW_4_PIN);
		_delay_us(100);
		if(!(PIN(COL_0_PORT) & (1<<COL_0_PIN))) chr = '<';
		else if(!(PIN(COL_1_PORT) & (1<<COL_1_PIN))) chr = '^';
		else if(!(PIN(COL_2_PORT) & (1<<COL_2_PIN))) chr = 'V';
		else if(!(PIN(COL_3_PORT) & (1<<COL_3_PIN))) chr = '>';

		if(chr != 0) hold = 1;
	}
	if(hold){
		PORT(ROW_0_PORT) &= ~(1<<ROW_0_PIN);
		PORT(ROW_1_PORT) &= ~(1<<ROW_1_PIN);
		PORT(ROW_2_PORT) &= ~(1<<ROW_2_PIN);
		PORT(ROW_3_PORT) &= ~(1<<ROW_3_PIN);
		PORT(ROW_4_PORT) &= ~(1<<ROW_4_PIN);
		_delay_us(100);
		if(
			(PIN(COL_0_PORT) & (1<<COL_0_PIN)) &&
			(PIN(COL_1_PORT) & (1<<COL_1_PIN)) &&
			(PIN(COL_2_PORT) & (1<<COL_2_PIN)) &&
			(PIN(COL_3_PORT) & (1<<COL_3_PIN))){
			hold = 0;
			hold_time = 0;
		}
		else if(hold_time<100) hold_time++;
	}
}

ISR(TIMER1_COMPA_vect){
	static uint8_t cnt_int = 0, state = 0;
	if(!cnt_int){
		MBRUSH_TRIGGER_TOGGLE;

		if(!state){
//			MBRUSH_TRIGGER_LOW;
//			cnt_int = MBRUSH_TRIGGER_TIME_US / 20 - 1; // wychodzi 0
//			state = 1;
			spi_byte_idx = 0;
		}
		else{
//			MBRUSH_TRIGGER_HIGH;
//			state = 0;
			if(work_mode){
	//				ST_LED_RED;
				TIMSK1 &= ~(1 << OCIE1A);
	//				cnt_int = 0; // jest r�wny 0
			}
			else cnt_int = interval_int;
		}
		state = ~state;
	}
	else cnt_int--;
}

ISR(TIMER1_COMPB_vect){
	if(photo_delay_cnt) photo_delay_cnt--;
}

ISR(INT0_vect){
	if(photo_delay_cnt) photo_delay_cnt--;
	else if(interval_send){
		if(interval_cnt_int) interval_cnt_int--;
		else{
//			trigger_req = 1;
			TCNT1 = 0;
			TIMSK1 |= (1 << OCIE1A); // on trigger time count, interwa� wysy�ania
			interval_cnt_int = interval_int;
		}
	}
}

//ISR(INT1_vect){
//	if(photo_delay_cnt) photo_delay_cnt--;
//	else if(interval_send){
//		if(interval_cnt_int) interval_cnt_int--;
//		else{
////			trigger_req = 1;
//			TCNT1 = 0;
//			TIMSK1 |= (1 << OCIE1A); // on trigger time count, interwa� wysy�ania
//			interval_cnt_int = interval_int;
//		}
//	}
//}

ISR(INT2_vect){
	if(!photo_detect) photo_detect = 1;
}

ISR(TIMER2_OVF_vect){
	static uint8_t byte, cnt;
	if(!cnt){
		printing_status = ~printing_status;
		cnt = 33;
	}
	cnt--;
	if(uart0_available() == 2){
		byte = uart0_getc();
		if(byte == 0x01){ // length
			byte = uart0_getc();
			if(byte == 0x01){ // reset
				lcd_cls();
				lcd_locate(0, 0);
				lcd_str("    Updating    ");
				lcd_locate(1, 0);
				lcd_str("    Flash...    ");
				WDTCSR |= (1<<WDE);
				wdt_enable(WDTO_120MS);
				_delay_ms(150);
			}
			else if(byte == 0x02){ // version
				uart0_putc(0x04);
				uart0_putc(VERSION_HW_H);
				uart0_putc(VERSION_HW_L);
				uart0_putc(VERSION_FW_H);
				uart0_putc(VERSION_FW_L);
			}
		}
		else uart0_flush();
	}
	else if(uart0_available() > 2) uart0_flush();
}
