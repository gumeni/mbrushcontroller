/*
 * defines.h
 *
 *  Created on: 3 lis 2021
 *      Author: dkopec
 */

#ifndef DEFINES_H_
#define DEFINES_H_

#define VERSION_HW_H	1	// max 9
#define VERSION_HW_L	0	// max 99
#define VERSION_FW_H	1	// max 9
#define VERSION_FW_L	7	// max 99

// Makra upraszczaj�ce dost�p do port�w
// *** PORT
#define PORT(x) SPORT(x)
#define SPORT(x) (PORT##x)
// *** PIN
#define PIN(x) SPIN(x)
#define SPIN(x) (PIN##x)
// *** DDR
#define DDR(x) SDDR(x)
#define SDDR(x) (DDR##x)

#define LCD_BKG_LIGHT_PORT		A
#define LCD_BKG_LIGHT_PIN		0
#define LCD_BKG_ON				PORT(LCD_BKG_LIGHT_PORT) |= (1 << LCD_BKG_LIGHT_PIN)
#define LCD_BKG_OFF				PORT(LCD_BKG_LIGHT_PORT) &= ~(1 << LCD_BKG_LIGHT_PIN)
#define LCD_BKG_OUT				DDR(LCD_BKG_LIGHT_PORT) |= (1 << LCD_BKG_LIGHT_PIN)

#define MBRUSH_BTN_PORT			B
#define MBRUSH_BTN_PIN			0
#define MBRUSH_BTN_HIGH			PORT(MBRUSH_BTN_PORT) |= (1 << MBRUSH_BTN_PIN)
#define MBRUSH_BTN_LOW			PORT(MBRUSH_BTN_PORT) &= ~(1 << MBRUSH_BTN_PIN)
#define MBRUSH_BTN_OUT			DDR(MBRUSH_BTN_PORT) |= (1 << MBRUSH_BTN_PIN)
#define MBRUSH_BTN_TIME_MS		90
#define PHOTO_PORT				B
#define PHOTO_PIN				2
#define PHOTO_PULLUP			PORT(PHOTO_PORT) |= (1 << PHOTO_PIN)

#define ENC_A_PORT				D
#define ENC_A_PIN				2
#define ENC_B_PORT				D
#define ENC_B_PIN				3
#define ENC_A_PULLUP			PORT(ENC_A_PORT) |= (1 << ENC_A_PIN)
#define ENC_B_PULLUP			PORT(ENC_B_PORT) |= (1 << ENC_B_PIN)

#define ST_LED_PORT_1			D
#define ST_LED_PIN_1			6
#define ST_LED_PORT_2			D
#define ST_LED_PIN_2			4
//#define ST_LED_RED			PORT(ST_LED_PORT) |= (1 << ST_LED_PIN)
//#define ST_LED_GREEN			PORT(ST_LED_PORT) &= ~(1 << ST_LED_PIN)
#define ST_LED_OUT				DDR(ST_LED_PORT_1) |= (1 << ST_LED_PIN_1); DDR(ST_LED_PORT_2) |= (1 << ST_LED_PIN_2);
#define ST_LED_OFF				DDR(ST_LED_PORT_1) &= ~(1 << ST_LED_PIN_1); DDR(ST_LED_PORT_2) &= ~(1 << ST_LED_PIN_2);
//#define ST_LED_BLUE				PORT(ST_LED_PORT_1) |= (1 << ST_LED_PIN_1); PORT(ST_LED_PORT_2) |= (1 << ST_LED_PIN_2)
////#define ST_LED_GREEN			PORT(ST_LED_PORT_1) |= (1 << ST_LED_PIN_1); PORT(ST_LED_PORT_2) &= ~(1 << ST_LED_PIN_2)
//#define ST_LED_RED				PORT(ST_LED_PORT_1) &= ~(1 << ST_LED_PIN_1); PORT(ST_LED_PORT_2) &= ~(1 << ST_LED_PIN_2)
//#define ST_LED_GREEN			PORT(ST_LED_PORT_1) &= ~(1 << ST_LED_PIN_1); PORT(ST_LED_PORT_2) |= (1 << ST_LED_PIN_2)

#define ST_LED_RED				PORT(ST_LED_PORT_1) |= (1 << ST_LED_PIN_1); PORT(ST_LED_PORT_2) &= ~(1 << ST_LED_PIN_2)
#define ST_LED_BLUE				PORT(ST_LED_PORT_1) &= ~(1 << ST_LED_PIN_1); PORT(ST_LED_PORT_2) |= (1 << ST_LED_PIN_2)
#define ST_LED_GREEN			PORT(ST_LED_PORT_1) |= (1 << ST_LED_PIN_1); PORT(ST_LED_PORT_2) |= (1 << ST_LED_PIN_2)
#define ST_LED_TOG				PORT(ST_LED_PORT_1) ^= (1 << ST_LED_PIN_1); PORT(ST_LED_PORT_2) |= (1 << ST_LED_PIN_2)

#define ROW_0_PORT				C
#define ROW_0_PIN				2
#define ROW_1_PORT				C
#define ROW_1_PIN				3
#define ROW_2_PORT				C
#define ROW_2_PIN				5
#define ROW_3_PORT				C
#define ROW_3_PIN				6
#define ROW_4_PORT				C
#define ROW_4_PIN				7
#define COL_0_PORT				C
#define COL_0_PIN				4
#define COL_1_PORT				C
#define COL_1_PIN				1
#define COL_2_PORT				C
#define COL_2_PIN				0
#define COL_3_PORT				D
#define COL_3_PIN				7

#define ROW_0_OUT				DDR(ROW_0_PORT) |= (1 << ROW_0_PIN)
#define ROW_1_OUT				DDR(ROW_1_PORT) |= (1 << ROW_1_PIN)
#define ROW_2_OUT				DDR(ROW_2_PORT) |= (1 << ROW_2_PIN)
#define ROW_3_OUT				DDR(ROW_3_PORT) |= (1 << ROW_3_PIN)
#define ROW_4_OUT				DDR(ROW_4_PORT) |= (1 << ROW_4_PIN)

#define	COL_0_PULLUP			PORT(COL_0_PORT) |= (1 << COL_0_PIN)
#define	COL_1_PULLUP			PORT(COL_1_PORT) |= (1 << COL_1_PIN)
#define	COL_2_PULLUP			PORT(COL_2_PORT) |= (1 << COL_2_PIN)
#define	COL_3_PULLUP			PORT(COL_3_PORT) |= (1 << COL_3_PIN)

#endif /* DEFINES_H_ */
